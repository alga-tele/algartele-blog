which node || {
    echo "node not installed"
    exit
}

filename=`date +%s`;

new_script=`cat << EOF
const fs = require('fs');

const page = './data/articles.json';

const list = JSON.parse(fs.readFileSync(page, 'utf8') || 'null') || p[];

const date = new Date({{timestamp}}000);
const name = '{{timestamp}}.md';

list.push({
  url : 'articles/'+name,
  fileName : name,
  fileTimestamp : new Date().getTime(),
  name: null,
});

fs.writeFileSync(page, JSON.stringify(list, null, 2));
EOF
`;

echo $new_script | sed -e "s/{{timestamp}}/${filename}/g" - | node -

touch data/articles/${filename}.md
